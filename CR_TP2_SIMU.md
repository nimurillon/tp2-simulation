# <center>Compte-Rendu TP2 Simulation</center>

## Nicolas MURILLON

### Sommaire : 

* Introduction
* Génération de nombres aléatoires uniformes entre A et B  
* Reproduction de distributions empiriques discrètes
* Reproduction de distributions continues
* Simulation de loi de distribution non inversible


----
## Introduction
----

L'objectif de ce TP est de générer des nombres aléatoires ainsi que de simuler des variables aléatoires à l'aide du générateur Mersenne Twister.  
  
Dans un premier temps nous allons vérifier que la sortie obtenue est celle attendue : 

```bash
$ gcc mt19937ar.c -o prog -g -Wall -Wextra -lm
$ ./prog > compare.txt
$ diff compare.txt mt19937ar.out
$ echo $?
0
```

Ici la commande ***diff*** renvoie 0, cela signifie que le contenu des fichiers ***compare.txt*** et ***mt19937ar.out*** est le même, nous venons donc de vérifier la répétabilité des nombres générés.

---
## Génération de nombres aléatoires uniformes entre a et b
---

Dans cette partie nous allons implémenter un générateur de nombres aléatoires uniforme entre a et b (a < b). La fonction se nomme ***uniform*** et prend en paramètre deux réels a et b avec a < b.  

Pour générer un nombre aléatoire entre a et b nous allons commencer par générer un nombre aléatoire entre 0 et 1 avec la fonction ```genrand_real1```, nous nommerons ce nombre ***seed***. Il suffit ensuite de multiplier ***seed*** par (b-a) puis ajouter a : 

```c
/* -------------------------------------------------------------- */
/* uniform : retourne un nombre aléatoire entre deux réels a et b */
/* Entrée : deux réels a et b                                     */
/* Sortie : un nombre aléatoire entre a et b                      */
/* -------------------------------------------------------------- */

double uniform(double a, double b) {
    double seed = genrand_real1();

    return a + (b-a)*seed;
}
```


---
## Reproduction de distributions empiriques discrètes
---

Pour reproduire (simuler) une population d'individus ayant la même distribution, nous pouvons utiliser la procédure suivante en utilisant un générateur de nombres pseudo-aléatoires uniformes entre 0 et 1.  

Supposons que nous ayons des données de terrain avec 3 classes : 500 observations dans la classe A, 100 dans la classe B et 400 dans la classe C. Ceci donne la probabilité de distribution suivante de 3 espèces (A, B et C) : 50% pour A, 10% pour B et 40% pour C.  
Lorsque nous tirons un nombre aléatoire, si ce nombre est strictement inférieur à 0,5, nous pouvons considérer que l'individu est de la catégorie A (nous l'appellerons classe A pour l'espèce A). Si le nombre est compris entre 0,5 et 0,6 (strictement), la personne est considérée comme appartenant à la classe B et si le nombre tiré est supérieur à 0,6, la personne est inscrite dans la Classe C.  

Dans un premier temps nous allons implémenter une fonction permettant de simuler cette distribution discrète avec les 3 classes A, B et C. Nous testerons cette fonction pour 1000 tirages puis pour 1000000 de tirages en affichant les pourcentages d'individus obtenus pour chaque espèce.

```c

/* ------------------------------------------------------------------- */
/* discrete_3_species : Simule la distribution discrète pour 3 classes */
/* Entrée : l'entier drawings correspondant au nombre de tirages       */
/* ------------------------------------------------------------------- */

void discrete_3_species(int drawings){
    int res[3]={0, 0, 0}; //Nombre d'individus tirés pour chaque classe
    int i;
    double seed;

    for(i=0;i<drawings;i++){
        seed = genrand_real1(); //tirage d'un individu

        if(seed < 0.5){
            res[0]++;
        }
        else if(seed <0.6){
            res[1]++;
        }
        else {
            res[2]++;
        }
    }

    printf("Percentage obtained per species for %d drawings:\n", drawings); 
    printf("A : %f\nB : %f\nC : %f\n\n", 100*(float)res[0]/drawings, 100*(float)res[1]/drawings, 100*(float)res[2]/drawings);
}
```

Voici ce que donne le résultat pour 1000 tirages et 1000000 tirages respectivement :

```bash
Percentage obtained per species for 1000 drawings:
A : 49.000000
B : 8.900000
C : 42.099998

Percentage obtained per species for 1000000 drawings:
A : 49.981499
B : 10.051400
C : 39.967098
```

Les distributions obtenues sont proches des distributions théoriques.  

Nous allons maintenant implémenter une fonction plus générique. Dans un premier temps nous allons calculer la probabilité d'être dans chaque classe, ensuite nous calculerons les probabilités cumulées dans un tableau et ensuite nous simulerons une population avec un certains nombre d'individus :

```c
/* -------------------------------------------------------------------- */
/* discrete_generic : Simule la distribution discrète pour n classes    */
/* Entrée : l'entier drawings correspondant au nombre de tirages        */
/*          l'entier size, le nombre de classes                         */
/*          le tableau d'entiers array du nombre d'individus par classe */
/*          le tableau cumulative des probabilités cumulées             */
/* -------------------------------------------------------------------- */

int generic_discrete(int size, int array[], double cumulative[], int drawings){
    int i,j;
    int sum=0; //Nombre total d'individus de l'échantillon
    float distribution[size]; //Tableau des probabilités d'appartenir à chaque classe
    int res[size];
    double seed;

    for(i=0; i<size;i++){
        sum+= array[i];
        res[i]=0; // Initialisation du nombre d'individus tirés pour chaque classe
    }

    for(i=0; i<size; i++){
        distribution[i] = (float) array[i]/(float)sum;
    }

    cumulative[0]=distribution[0];

    for(i=1;i<size;i++){
        cumulative[i]= cumulative[i-1] + distribution[i]; // Calcul des probabilités cumulées
    }

    for(i=0;i<drawings;i++){
        seed = genrand_real1(); // tirage d'un individu
        j=0;

        while(seed >= cumulative[j]){ // Recherche de la classe de l'individu
            j++;
        }
        res[j]++;
    }

    printf("Percentage obtained per species for %d drawings:\n", drawings);
    for(i=0;i<size;i++){
        printf("Species %d : %f\n", i, 100*(float)res[i]/drawings);
    }


    return 0;
}
```

Nous allons tester cette fonction avec les données de la diapositive 75 du cours :

```c

int main(void)
{
    int i;
    int size=6;
    int array[]={100,400,600,400,100,200};
    double cumulative[size];
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
    
    generic_discrete(size,array,cumulative,1000);
    generic_discrete(size,array,cumulative,1000000);

    return 0;
```

Nous obtenons alors le résultat suivant :

```bash
Percentage obtained per species for 1000 drawings:
Species 0 : 5.600000
Species 1 : 22.500000
Species 2 : 31.400000
Species 3 : 23.600000
Species 4 : 6.100000
Species 5 : 10.800000

Percentage obtained per species for 1000000 drawings:
Species 0 : 5.548300
Species 1 : 22.245300
Species 2 : 33.339802
Species 3 : 22.198000
Species 4 : 5.551900
Species 5 : 11.116700
```

Encore une fois, les pourcentages obtenus se rapprochent des probabilités obtenus avec l'échantillon, d'autant plus que le nombre de tirages augmente.

---
## Reproduction de distributions continues
---

L'objectif de cette partie est de simuler une loi exponentielle négative. Pour se faire nous allons prendre en paramètre la moyenne souhaitée :

```c

/* -------------------------------------------------------------------- */
/* neg_exp : fonction inverse d'une loi exponentielle négative uniforme */
/* Entrée : un réel mean                                                */
/* Sortie : un nombre généré aléatoirement suivant une loi              */
/*          exponentielle négative                                      */
/* -------------------------------------------------------------------- */

double neg_exp(double mean){
    double seed = genrand_real1();

    return -mean*log(1-seed);
}
```

Nous allons maintenant tester cette fonction avec une moyenne de 10 : 

```c

/* --------------------------------------------------------------------- */
/* test_neg_exp : effectue drawings nombre d'appel à la fonction neg_exp */
/* Entrée : un réel mean (moyenne) et un entier drawings pour le nombre  */
/*          de tirages à effectuer                                       */
/* Sortie : La moyenne des nombres tirés                                 */
/* --------------------------------------------------------------------- */

double test_neg_exp(double mean, int drawings){
    double sum=0.;
    int i;

    for(i=0; i<drawings; i++){
        sum = sum + neg_exp(mean);
    }

    return sum/(double)drawings; // moyenne obtenue pour drawings tirages
}
```

Nous obtenons alors le résultat suivant :
```bash
Average after 1000 drawings : 10.090822 
Average after 1000000 drawings : 10.000759
```

Ces moyennes sont bien proches de 10, les résultats sont donc cohérents  

Nous allons maintenant regarder la distribution discrète en utilisant un tableau de 20 cases. Nous regarderons la fréquence des nombres entre 0 et 1, entre 1 et 2...

```c

/* --------------------------------------------------------------------- */
/* test_bin : regarde la fréquence des nombres tirés par neg_exp         */
/* Entrée : un réel mean (moyenne) et un entier drawings pour le nombre  */
/*          de tirages à effectuer                                       */
/* Sortie : Le tableau Test20bins contenant le nombre d'individus        */
/*          dans chaque intervalle                                       */
/* --------------------------------------------------------------------- */

void test_bin(int Test20bins[], int mean, int drawings){
    int i;
    double nexp;

    for(i=0;i<20;i++){
        Test20bins[i]=0; // Initialisation du nombre d'individus dans chaque intervalle
    }
    
    for(i=0;i<drawings;i++){
        nexp = neg_exp(mean);
        if(nexp>=21){
            nexp=20.;
        }
        Test20bins[ (int) nexp ] ++;
    }
}
```

Nous obtenons le résultat suivant :



---
## Simulation de loi de distribution non inversible
---

Dans cette partie nous allons implémenter la méthode de Box et Muller des nombres aléatoires suivant une loi Normale centrée réduite :

```c

/* --------------------------------------------------------------------- */
/* Box_Muller : génère des nombres aléatoires suivant une loi Normale    */
/*              centrée réduite                                          */
/* Entrée : l'entier drawings du nombre de tirages à effectuer           */
/* Sortie : Le tableau Test20Bins contenant le nombre d'individus        */
/*          dans chaque intervalle                                       */
/* --------------------------------------------------------------------- */

void Box_Muller(int Test20Bins[], int drawings){
    double Rn1, Rn2, x1,x2;
    int i;

    for(i=0;i<20;i++){
        Test20Bins[i]=0; //Initialisation du tableau résultat
    }

    for(i=0;i<drawings/2;i++){
        Rn1 = genrand_real1();
        Rn2 = genrand_real2();

        x1=cos(2*M_PI*Rn2)*sqrt(-2*log(Rn1));
        x2=sin(2*M_PI*Rn2)*sqrt(-2*log(Rn1));

        if(x1<1 && x1>-1){
            Test20Bins[(int) ((x1+1)*10)]++;
        }
        
        if(x2>-1 && x2<1){
            Test20Bins[(int) ((x2+1)*10)]++;
        }
    }
}
```