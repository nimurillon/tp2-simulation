/* 
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)  
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

#include <stdio.h>
#include <math.h>

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

static unsigned long mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

/* initializes mt[N] with a seed */
void init_genrand(unsigned long s)
{
    mt[0]= s & 0xffffffffUL;
    for (mti=1; mti<N; mti++) {
        mt[mti] = 
	    (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti); 
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        mt[mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void init_by_array(unsigned long init_key[], int key_length)
{
    int i, j, k;
    init_genrand(19650218UL);
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525UL))
          + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941UL))
          - i; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */ 
}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long genrand_int32(void)
{
    unsigned long y;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
        int kk;

        if (mti == N+1)   /* if init_genrand() has not been called, */
            init_genrand(5489UL); /* a default initial seed is used */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        mti = 0;
    }
  
    y = mt[mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

/* generates a random number on [0,0x7fffffff]-interval */
long genrand_int31(void)
{
    return (long)(genrand_int32()>>1);
}

/* generates a random number on [0,1]-real-interval */
double genrand_real1(void)
{
    return genrand_int32()*(1.0/4294967295.0); 
    /* divided by 2^32-1 */ 
}

/* generates a random number on [0,1)-real-interval */
double genrand_real2(void)
{
    return genrand_int32()*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
double genrand_real3(void)
{
    return (((double)genrand_int32()) + 0.5)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
double genrand_res53(void) 
{ 
    unsigned long a=genrand_int32()>>5, b=genrand_int32()>>6; 
    return(a*67108864.0+b)*(1.0/9007199254740992.0); 
} 
/* These real versions are due to Isaku Wada, 2002/01/09 added */



/* Les fonctions qui suivent sont celles de lab2 */


/* -------------------------------------------------------------- */
/* uniform : retourne un nombre aléatoire entre deux réels a et b */
/* Entrée : deux réels a et b                                     */
/* Sortie : un nombre aléatoire entre a et b                      */
/* -------------------------------------------------------------- */

double uniform(double a, double b) {
    double seed = genrand_real1();

    return a + (b-a)*seed;
}

/* ------------------------------------------------------------------- */
/* discrete_3_species : Simule la distribution discrète pour 3 classes */
/* Entrée : l'entier drawings correspondant au nombre de tirages       */
/* ------------------------------------------------------------------- */

void discrete_3_species(int drawings){
    int res[3]={0, 0, 0};
    int i;
    double seed;

    for(i=0;i<drawings;i++){
        seed = genrand_real1();

        if(seed < 0.5){
            res[0]++;
        }
        else if(seed <0.6){
            res[1]++;
        }
        else {
            res[2]++;
        }
    }

    printf("Percentage obtained per species for %d drawings:\n", drawings); 
    printf("A : %f\nB : %f\nC : %f\n\n", 100*(float)res[0]/drawings, 100*(float)res[1]/drawings, 100*(float)res[2]/drawings);
}

/* -------------------------------------------------------------------- */
/* discrete_generic : Simule la distribution discrète pour n classes    */
/* Entrée : l'entier drawings correspondant au nombre de tirages        */
/*          l'entier size, le nombre de classes                         */
/*          le tableau d'entiers array du nombre d'individus par classe */
/*          le tableau cumulative des probabilités cumulées             */
/* -------------------------------------------------------------------- */

int generic_discrete(int size, int array[], double cumulative[], int drawings){
    int i,j;
    int sum=0;
    float distribution[size];
    int res[size];
    double seed;

    for(i=0; i<size;i++){
        sum+= array[i];
        res[i]=0;
    }

    for(i=0; i<size; i++){
        distribution[i] = (float) array[i]/(float)sum;
    }

    cumulative[0]=distribution[0];

    for(i=1;i<size;i++){
        cumulative[i]= cumulative[i-1] + distribution[i];
    }

    for(i=0;i<drawings;i++){
        seed = genrand_real1();
        j=0;

        while(seed >= cumulative[j]){
            j++;
        }
        res[j]++;
    }

    printf("Percentage obtained per species for %d drawings:\n", drawings);
    for(i=0;i<size;i++){
        printf("Species %d : %f\n", i, 100*(float)res[i]/drawings);
    }


    return 0;
}

double neg_exp(double mean){
    double seed = genrand_real1();

    return -mean*log(1-seed);
}

double test_neg_exp(double mean, int drawings){
    double sum=0.;
    int i;

    for(i=0; i<drawings; i++){
        sum = sum + neg_exp(mean);
    }

    return sum/(double)drawings; 
}

void test_bin(int Test20bins[], int mean, int drawings){
    int i;
    double nexp;

    for(i=0;i<21;i++){
        Test20bins[i]=0;
    }
    
    for(i=0;i<drawings;i++){
        nexp = neg_exp(mean);
        if(nexp>=20){
            nexp=20.;
        }
        Test20bins[(int)nexp]++;
    }
}

void Box_Muller(int Test20Bins[], int drawings){
    double Rn1, Rn2, x1,x2;
    int i;

    for(i=0;i<20;i++){
        Test20Bins[i]=0; //Initialisation du tableau résultat
    }

    for(i=0;i<drawings/2;i++){
        Rn1 = genrand_real1();
        Rn2 = genrand_real2();

        x1=cos(2*M_PI*Rn2)*sqrt(-2*log(Rn1));
        x2=sin(2*M_PI*Rn2)*sqrt(-2*log(Rn1));

        if(x1<1 && x1>-1){
            Test20Bins[(int) ((x1+1)*10)]++;
        }
        
        if(x2>-1 && x2<1){
            Test20Bins[(int) ((x2+1)*10)]++;
        }
    }
}

int main(void)
{
    int i;
    int size=6;
    int array[]={100,400,600,400,100,200};
    double cumulative[size];
    int Test20bins[21];
    int Test20Bins[20];
    int sum=0;
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
    /* printf("1000 outputs of genrand_int32()\n");
    for (i=0; i<1000; i++) {
      printf("%10lu ", genrand_int32());
      if (i%5==4) printf("\n");
    }
    printf("\n1000 outputs of genrand_real2()\n");
    for (i=0; i<1000; i++) {
      printf("%10.8f ", genrand_real2());
      if (i%5==4) printf("\n");
    }  */

    /* discrete_3_species(1000);    
    discrete_3_species(1000000); */
   /*  generic_discrete(size,array,cumulative,1000);
    generic_discrete(size,array,cumulative,1000000); */

    /* printf("Average after 1000 drawings : %lf \n",test_neg_exp(10.,1000));
    printf("Average after 1000000 drawings : %lf \n",test_neg_exp(10.,1000000)); */
/* 

    test_bin(Test20bins, 10, 1000);
    printf("Nombre d'éléments tirés dans chaque intervalle pour 1000 tirages\n");
    for(i=0;i<20;i++) {
        printf("Nombre d'elements tires entre %d et %d : %d\n", i, i+1, Test20bins[i]);
    }

    test_bin(Test20bins, 10, 1000000);
    printf("Nombre d'éléments tirés dans chaque intervalle pour 1000000 tirages\n");
    for(i=0;i<20;i++) {
        printf("Nombre d'elements tires entre %d et %d : %d\n", i, i+1, Test20bins[i]);
    }  */
    Box_Muller(Test20Bins, 1000);

    for(i=0;i<20;i++) {
        printf("Nombre d'elements tires entre %f et %f : %d\n", (float)i /10 -1, (float)(i+1)/10-1, Test20Bins[i]);
        sum+=Test20Bins[i];
    }

    printf("Pourcentage entre -1 et 1 pour 1000 tirages : %f\n", (float)sum/10);

    Box_Muller(Test20Bins, 1000000);
    sum=0;
    for(i=0;i<20;i++) {
        printf("Nombre d'elements tires entre %f et %f : %d\n", (float)i /10 -1, (float)(i+1)/10-1, Test20Bins[i]);
        sum+=Test20Bins[i];
    }
    printf("Pourcentage entre -1 et 1 pour 1000000 tirages : %f\n", (float)sum/10000); 
    return 0;
}
